var userlist = userlist || {};
var userlist_data = userlist_data || {};
var wp = window.wp || {};

jQuery(document).ready(function () {

	if ('undefined' !== wp.api) {

		var $ = jQuery;

		userlist.UsersView = wp.Backbone.View.extend({

			template: wp.template('user-list'),
			el: '#users-table',
			bodyEl: '#users',
			rolesEl: '#user_roles',
			filters: {},
			filtersDefault: {
				orderby: 'name',
				order: 'asc',
				page: 1,
				per_page: 10,
				context: 'edit',
			},

			events: {

				'click .refresh': function () {
					return this.fetchCollection({});
				},

				'change #users_per_page': function () {
					return this.fetchCollection({});
				},

				'click .clear': function () {
					$("#user_roles").val(0);
					$("#users_per_page").val(this.filtersDefault.per_page);
					$('.get_page').removeClass('selected-page').prop('disabled', false);
					$('.order_users').removeClass('selected-order').prop('disabled', false);
					delete this.filters.orderby_custom;
					this.filters = Object.assign({}, this.filtersDefault);
					return this.fetchCollection({});
				},

				'click .get_page': function (event) {
					$el = $(event.currentTarget);
					$('.get_page').removeClass('selected-page').prop('disabled', false);
					$el.addClass('button-secondary').prop('disabled', true);
					var page = $(event.currentTarget).data('page');
					this.filters.page = page;
					this.fetchCollection({page: page});
				},

				'change #user_roles': function (event) {

					var user_role = $("#user_roles").val();
					if (0 != user_role) {
						this.filters.roles = user_role;
						this.filters.page = this.filtersDefault.page;
					} else {
						delete this.filters.roles;
					}
					this.fetchCollection();
				},

				'click .order_users': function (event) {

					$el = $(event.currentTarget);
					this.filters.order = $el.data('order');
					if ($el.data('orderby') === 'username') {
						this.filters.orderby_custom = 'user_login';
					} else {
						this.filters.orderby = $el.data('orderby');
						delete this.filters.orderby_custom;
					}
					$('.order_users').removeClass('selected-order').prop('disabled', false);
					$el.addClass('selected-order').prop('disabled', true);
					this.fetchCollection();
				},

				'message:add #users_notices': function (event, options) {
					var $el = $("<div>", {"class": options.type, text: options.message});
					$('#users_notices').append($el);
					setTimeout(
						function () {
							$el.remove();
						},
						3000
					);
				}
			},

			addMessage: function (message, type) {
				$('#users_notices').trigger('message:add', {message: message, type: type});
			},

			showSpinner: function () {
				$('#users-table').addClass('block-ui');
				$('#users-table').animate({opacity: 0.5});
				$('#users_loader').removeClass('hidden');
			},

			hideSpinner: function () {
				$('#users-table').animate({opacity: 1});
				$('#users-table').removeClass('block-ui');
				$('#users_loader').addClass('hidden');
			},


			initialize: function () {
				this.filters = Object.assign({}, this.filtersDefault);
				this.listenTo(this.collection, 'reset', this.renderCollection);
			},

			prepareFilters: function (options) {

				filters = this.filters;

				var per_page = $("#users_per_page").val();
				var user_role = $("#user_roles").val();
				if ('0' !== user_role) {
					filters.roles = user_role;
				} else {
					delete filters.roles;
				}
				if (0 !== parseInt(per_page)) {
					filters.per_page = parseInt(per_page);
				}
				var filters = Object.assign({}, filters, options);
				return filters;
			},

			fetchCollection: function (options) {
				options = this.prepareFilters(options);
				this.showSpinner();
				this.collection.fetch({
					data: options,
					reset: true,
				}).then(this.successHandler.bind(this), this.errorHandler.bind(this));
			},

			successHandler: function () {
				this.hideSpinner();
				this.addMessage('Fetched results!', 'updated');
			},
			errorHandler: function (a) {
				this.hideSpinner();
				this.addMessage(a.responseJSON.message, 'error');
			},

			populatePagination: function (c, m) {
				var current = c,
					last = m,
					delta = 2,
					left = current - delta,
					right = current + delta + 1,
					range = [],
					rangeWithDots = [],
					l;

				for (let i = 1; i <= last; i++) {
					if (i == 1 || i == last || i >= left && i < right) {
						range.push(i);
					}
				}

				for (let i of range) {
					if (l) {
						if (i - l === 2) {
							rangeWithDots.push(l + 1);
						} else if (i - l !== 1) {
							rangeWithDots.push('...');
						}
					}
					rangeWithDots.push(i);
					l = i;
				}

				return rangeWithDots;
			},

			renderPagination: function (range, total) {
				var pagination_tmpl = _.template($('#tmpl-user-pagination').html());
				var pagination_desc_tmpl = _.template($('#tmpl-user-pagination-description').html());
				var pagination_html = pagination_tmpl({data: range, currentPage: this.filters.page});
				var pagination_desc_html = pagination_desc_tmpl(total);
				$('#user_pages').html(pagination_html);
				$('#users_pagination').html(pagination_desc_html);
			},

			render: function (a) {
				this.$el.html(this.template());
				var tm = _.template($('#tmpl-user-roles').html());
				$(this.rolesEl).append(tm({data: this.roles}));
				return this;
			},

			renderCollection: function () {
				var totalPages = this.collection.state.totalPages;
				var totalObjects = this.collection.state.totalObjects;
				var currentPage = this.collection.state.currentPage;
				var perPage = this.filters.per_page;
				var range = this.populatePagination(currentPage, totalPages);
				this.renderPagination(range, {
					start: (currentPage - 1) * perPage + 1,
					end: totalObjects < currentPage * perPage ? totalObjects : currentPage * perPage,
					total: totalObjects
				});

				$(this.bodyEl).html('');
				this.collection.each(function (user) {
					var userView = new userlist.UserView({model: user});
					$(this.bodyEl).append(userView.render().el)
				}, this);

				return this;
			},
		});

		userlist.UserView = wp.Backbone.View.extend({
			template: wp.template('user-row'),
			tagName: 'tr',

			render: function () {
				this.$el.html(this.template(this.model.toJSON()));
				return this;
			}
		});

		userlist.initialize = function () {
			usersCollection = new wp.api.collections.Users();
			userlist.usersView = new userlist.UsersView({collection: usersCollection});
			userlist.usersView.roles = userlist_data.roles;
			userlist.usersView.render();
			userlist.usersView.fetchCollection();
		};

		wp.api.loadPromise.done(function () {
			userlist.initialize();
		});
	}

});
