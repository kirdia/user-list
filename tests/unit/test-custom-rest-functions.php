<?php
/**
 * Class ApiTest
 *
 * Test fields rest endpoints.
 *
 * @package P4EN
 */

/**
 * ApiTest class.
 */
class CustomRestFunctionsTest extends WP_UnitTestCase {

	/**
	 * The REST server.
	 *
	 * @var WP_REST_Server
	 */
	protected $server;

	/**
	 * Slug for the API
	 *
	 * @var string
	 */
	protected $api_slug = 'user-list';

	/**
	 * Set up variables and needed options for testing fields endpoints.
	 */
	public function setUp() {
		parent::setUp();

		global $wp_rest_server;
		$wp_rest_server = new \WP_REST_Server();
		$this->server   = $wp_rest_server;

		if ( ! defined( 'REST_REQUEST' ) ) {
			define( 'REST_REQUEST', true );
		}

		do_action( 'rest_api_init' );

		// Create a user with administrator role.
		$this->factory->user->create(
			[
				'role'       => 'administrator',
				'user_login' => 'ul_admin',
			]
		);
		$user = get_user_by( 'login', 'ul_admin' );
		wp_set_current_user( $user->ID );
		$this->create_users();
	}

	/**
	 * Test get users endpoint.
	 *
	 * @covers       \UserList\User_Report_Controller::add_users_param_to_endpoint
	 */
	public function test_get_users_endpoint() {

		$request  = new \WP_REST_Request( 'GET', '/wp/v2/users' );
		$response = $this->server->dispatch( $request );
		$this->assertEquals( 200, $response->get_status() );
		$data = $response->get_data();
		$this->assertEquals( 3, count( $data ) );
	}

	/**
	 * Test get users endpoint custom user ordering.
	 *
	 * @covers       \UserList\User_Report_Controller::add_users_param_to_endpoint
	 */
	public function test_add_orderby_param() {

		$request = new \WP_REST_Request( 'GET', '/wp/v2/users' );
		$request->set_query_params(
			[
				'orderby_custom' => 'user_login',
			]
		);
		$response = $this->server->dispatch( $request );
		$this->assertEquals( 200, $response->get_status() );
		$data = $response->get_data();
		$this->assertEquals( 3, count( $data ) );
	}

	/**
	 * Mock fields data provider.
	 */
	private function create_users() : void {
		// Create a user with editor role.
		$this->factory->user->create(
			[
				'role'       => 'author',
				'user_login' => 'ul_editor',
			]
		);
		$this->factory->user->create(
			[
				'role'       => 'editor',
				'user_login' => 'ul_editor',
			]
		);
		$this->factory->user->create(
			[
				'role'       => 'contributor',
				'user_login' => 'ul_editor',
			]
		);
	}
}
