=== User List ===
Contributors: kirdia
Tags: users, rest api
Requires at least: 4.7
Tested up to: 4.9.8
Requires PHP: 7.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Adds an admin page in users menu, where you can list WordPress users, filter by role and order by name or username.