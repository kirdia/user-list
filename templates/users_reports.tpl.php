<script type="text/template" id="tmpl-user-list">
	<div id="users_notices"></div>
	<table class="wp-list-table widefat">
		<thead>
		<tr>
			<th>Name
				<span class="dashicons dashicons-arrow-up-alt2 order_users order-field" data-orderby="name"
				      data-order="asc"></span>
				<span class="dashicons dashicons-arrow-down-alt2 order_users order-field" data-orderby="name"
				      data-order="desc"></span></th>
			<th>Username
				<span class="dashicons dashicons-arrow-up-alt2 order_users order-field" data-orderby="username"
				      data-order="asc"></span>
				<span class="dashicons dashicons-arrow-down-alt2 order_users order-field" data-orderby="username"
				      data-order="desc"></span>
			</th>
			<th>Role</th>
			<th>Description</th>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th><select id="user_roles"></select></th>
			<th></th>
		</tr>
		</thead>
		<tbody id="users"></tbody>
		<tfoot>
		<tr>
			<th id="users_pagination"></th>
			<th>Users per page: <input type="number" id="users_per_page" value="10" min="1" max="100"/></th>
			<th id="user_pages" colspan="2"></th>
		</tr>
		</tfoot>
	</table>
	<p>
		<button class="button button-primary refresh">Refresh</button>
		<button class="button clear">Clear</button>
	</p>
</script>

<script type="text/template" id="tmpl-user-row">
	<td><a href="{{{ data.link }}}">{{{ data.name }}}</a></td>
	<td>{{{ data.username }}}</td>
	<td>{{{ data.roles[0] }}}</td>
	<td>{{{ data.description }}}</td>
</script>

<script type="text/template" id="tmpl-user-pagination">
	<% _.each(data, function(page) { %>
		<% if (page > 0) { %>
			<a class="button get_page <% if ( page == currentPage ){ print('selected-page'); } %>" data-page="<%= page %>"><%= page %></a>
		<% } else { %>
			<%= page %>
		<% } %>
	<% }); %>
</script>

<script type="text/template" id="tmpl-user-pagination-description">
	<% if (total < 1) { %>
		No results
	<% } else { %>
		Showing <%= start %> to <%= end %> of <%= total %>
	<% } %>
</script>

<script type="text/template" id="tmpl-user-roles">
	<option value="0">All roles</option>
	<% _.each(data.roles, function(role, key) { %>
		<option value="<%= key %>"><%= role.name %></option>
	<% }); %>
</script>
