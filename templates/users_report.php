<h1>Users List Page</h1>
<div class="tablenav top">
	<p>Displays a user list table where users can be sorted by name or username and filtered by role.</p>
</div>

<div class="wrap">
	<h1>Users List
		<span class="hidden" id="users_loader">
			<img src="/wp-admin/images/wpspin_light.gif"/>
		</span>
	</h1>
	<div id="users-table"></div>
</div>
