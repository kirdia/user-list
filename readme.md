# WordPress User List Plugin

[![pipeline status](https://gitlab.com/kirdia/user-list/badges/master/pipeline.svg)](https://gitlab.com/kirdia/user-list/commits/master)
[![coverage report](https://gitlab.com/kirdia/user-list/badges/master/coverage.svg)](https://gitlab.com/kirdia/user-list/commits/master)

## Description

Adds an admin page in users menu, where you can list wordpress users, filter by role and order by name or username.


### Features
* List WordPress users
* Filter users per role
* Order users by name or username


## Installation

1. Unzip plugin's contents to WordPress plugins folder
2. Navigate to Plugins Page in your site.
3. Activate the User List plugin.
4. A new menu is created under Users default menu in the WordPress admin area.
