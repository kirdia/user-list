<?php
/**
 * User Report main class.
 *
 * @package UserList
 */

namespace UserList;

/**
 * Class User_Report_Controller
 */
class User_Report_Controller {

	/**
	 * Hooks all the needed functions to load the block.
	 */
	public function load() {
		$this->register_hooks();
	}

	/**
	 * Register actions for WordPress hooks and filters.
	 */
	private function register_hooks() {
		add_action( 'admin_menu', [ $this, 'add_users_report_admin_menu_item' ] );
		add_filter( 'rest_user_query', [ $this, 'add_users_param_to_endpoint' ], 10, 2 );
		add_action( 'admin_print_footer_scripts-users_page_user-list', [ $this, 'print_admin_footer_scripts' ], 1 );
	}

	/**
	 * Add extra orderby_custom column in users rest endpoint.
	 *
	 * @param array $args    Arguments array passed to endpoint.
	 * @param array $request Initial request parameters to the endpoint.
	 *
	 * @return mixed
	 */
	public function add_users_param_to_endpoint( $args, $request ) {
		if ( ! isset( $request['orderby_custom'] ) ) {
			return $args;
		}

		$allowed_orderby_fields = [ 'user_login' ];
		if ( in_array( $request['orderby_custom'], $allowed_orderby_fields, true ) ) {
			$args['orderby'] = $request['orderby_custom'];
		}

		return $args;
	}

	/**
	 * Add users report submenu item.
	 */
	public function add_users_report_admin_menu_item() : void {
		add_users_page(
			__( 'Users List', 'user-list' ),
			__( 'Users List', 'user-list' ),
			'read',
			'user-list',
			[ $this, 'render_users_report_page' ]
		);
	}

	/**
	 * Load underscore templates to footer.
	 */
	public function print_admin_footer_scripts() : void {
		echo $this->get_template( 'users_reports' ); //phpcs:ignore
	}

	/**
	 * Callback function to render users report page.
	 */
	public function render_users_report_page() : void {

		wp_enqueue_style( 'uslt_admin_style', plugins_url( USLT_PLUGIN_DIRNAME ) . '/assets/css/user-list.css', [], '1.0' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_register_script(
			'users-report',
			plugins_url( USLT_PLUGIN_DIRNAME ) . '/assets/js/users_report.js',
			[
				'jquery',
				'wp-api',
				'wp-backbone',
			],
			'1.0',
			true
		);
		wp_localize_script(
			'users-report',
			'userlist_data',
			[
				'api_url' => get_site_url() . '/wp-json/wp/v2',
				'nonce'   => wp_create_nonce( 'wp_rest' ),
				'roles'   => wp_roles(),
			]
		);
		wp_enqueue_script( 'users-report' );
		include USLT_PLUGIN_DIR . '/templates/users_report.php';
	}

	/**
	 * Get underscore template from filesystem.
	 *
	 * @param string $template Template name.
	 *
	 * @return string
	 */
	protected function get_template( $template ) {

		$template = USLT_PLUGIN_DIR . '/templates/' . $template . '.tpl.php';
		if ( file_exists( $template ) ) {
			$contents = file_get_contents( $template ); //phpcs:ignore

			return false !== $contents ? $contents : '';
		}

		return '';
	}
}

