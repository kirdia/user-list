<?php
/**
 * Loader class
 *
 * @package UserList
 * @since 1.0
 */

namespace UserList;

/**
 * Class Loader
 *
 * This class checks requirements and if all are met then it hooks the plugin.
 */
final class Loader {

	/** @var Loader $instance */
	private static $instance;
	/** @var array $services */
	private $services;
	/** @var string $required_php */
	private $required_php = USLT_REQUIRED_PHP;

	/**
	 * Makes sure there is only one instance during runtime.
	 *
	 * @param array $services The Controller services to inject.
	 *
	 * @return Loader
	 */
	public static function get_instance( $services = [] ): Loader {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self( $services );
		}

		return self::$instance;
	}

	/**
	 * Creates the plugin's loader object.
	 * Checks requirements and if its ok it hooks the hook_plugin method on the 'init' action which fires
	 * after WordPress has finished loading but before any headers are sent.
	 * Most of WP is loaded at this stage (but not all) and the user is authenticated.
	 *
	 * @param array $services The Controller services to inject.
	 */
	private function __construct( $services = [] ) {
		$this->load_services( $services );
		$this->check_requirements();
		add_action( 'plugins_loaded', [ $this, 'load_i18n' ] );
	}

	/**
	 * Loads all services registered from this plugin.
	 *
	 * @param array $services    The Controller services to inject.
	 */
	public function load_services( $services ) : void {
		$this->services = $services;

		if ( $this->services ) {
			foreach ( $this->services as $service ) {
				( new $service() )->load();
			}
		}
	}

	/**
	 * Hooks the plugin.
	 */
	private function hook_plugin() : void {
		add_action( 'admin_menu', [ $this, 'load_i18n' ] );
		// Provide hook for other plugins.
		do_action( 'uslt_plugin_loaded' );
	}

	/**
	 * Checks plugin requirements.
	 * If requirements are met then hook the plugin.
	 */
	private function check_requirements() : void {
		// Check if we are on the admin panel.
		if ( is_admin() ) {
			// Run the version check. If it is successful, continue with hooking under 'init' the initialization of this plugin.
			if ( $this->check_required_php() ) {
				$this->hook_plugin();
			} else {
				deactivate_plugins( USLT_PLUGIN_BASENAME );
				wp_die(
					'<div class="error fade">' .
					'<strong>' . esc_html__( 'PHP Requirements Error', 'user-list' ) . '</strong><br /><br />' . esc_html( USLT_PLUGIN_NAME . __( ' requires a newer version of PHP.', 'user-list' ) ) . '<br />' .
					'<br/>' . esc_html__( 'Minimum required version of PHP: ', 'user-list' ) . '<strong>' . esc_html( $this->required_php ) . '</strong>' .
					'<br/>' . esc_html__( 'Running version of PHP: ', 'user-list' ) . '<strong>' . esc_html( phpversion() ) . '</strong>' .
					'</div>',
					'Plugin Requirements Error',
					[
						'response'  => \WP_Http::OK, //phpcs:ignore
						'back_link' => true,
					]
				);
			}
		}
	}

	/**
	 * Check if the server's php version is less than the required php version.
	 *
	 * @return bool true if version check passed or false otherwise.
	 */
	private function check_required_php() : bool {
		return version_compare( phpversion(), $this->required_php, '>=' );
	}

	/**
	 * Load internationalization (i18n) for this plugin.
	 * Reference: http://codex.wordpress.org/I18n_for_WordPress_Developers
	 */
	public function load_i18n() : void {
		load_plugin_textdomain( 'user-list', false, USLT_PLUGIN_DIRNAME . '/languages/' );
	}

	/**
	 * Make clone magic method private, so nobody can clone instance.
	 */
	private function __clone() {
	}

	/**
	 * Make wakeup magic method private, so nobody can unserialize instance.
	 */
	private function __wakeup() {
	}
}
