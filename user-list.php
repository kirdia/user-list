<?php
/**
 * Plugin Name: User List
 * Description: Creates an admin page which renders a table list of WordPress users.
 * Version: 1.0
 * Php Version: 7.0
 *
 * Author: Kyriakos Diamantis
 * Text Domain: user-list
 *
 * License: GPLv3
 * Copyright (C) 2018 Kyriakos Diamantis
 *
 * @package Userlist
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || die( 'Direct access is forbidden !' );


/*
========================
	C O N S T A N T S
========================
*/
if ( ! defined( 'USLT_REQUIRED_PHP' ) ) {
	define( 'USLT_REQUIRED_PHP', '7.0' );
}

if ( ! defined( 'USLT_PLUGIN_BASENAME' ) ) {
	define( 'USLT_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
}
if ( ! defined( 'USLT_PLUGIN_DIRNAME' ) ) {
	define( 'USLT_PLUGIN_DIRNAME', dirname( USLT_PLUGIN_BASENAME ) );
}
if ( ! defined( 'USLT_PLUGIN_DIR' ) ) {
	define( 'USLT_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . USLT_PLUGIN_DIRNAME );
}
if ( ! defined( 'USLT_PLUGIN_NAME' ) ) {
	define( 'USLT_PLUGIN_NAME', 'User List' );
}

if ( ! defined( 'USLT_PLUGIN_SLUG_NAME' ) ) {
	define( 'USLT_PLUGIN_SLUG_NAME', 'user-list' );
}
if ( ! defined( 'USLT_INCLUDES_DIR' ) ) {
	define( 'USLT_INCLUDES_DIR', USLT_PLUGIN_DIR . '/includes/' );
}
if ( ! defined( 'USLT_ADMIN_DIR' ) ) {
	define( 'USLT_ADMIN_DIR', plugins_url( USLT_PLUGIN_DIRNAME . '/admin/' ) );
}

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	define( 'WP_UNINSTALL_PLUGIN', USLT_PLUGIN_BASENAME );
}

require_once ABSPATH . 'wp-admin/includes/plugin.php';
require_once __DIR__ . '/lib/classes/class-loader.php';
require_once __DIR__ . '/lib/classes/class-user-report-controller.php';


/*
==========================
	L O A D  P L U G I N
==========================
*/
UserList\Loader::get_instance(
	[
		'UserList\User_Report_Controller',
	]
);


